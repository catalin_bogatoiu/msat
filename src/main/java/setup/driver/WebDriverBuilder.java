package setup.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class WebDriverBuilder {
    public static ChromeDriverBuilder forChrome(){
        return new ChromeDriverBuilder();
    }

    public static class ChromeDriverBuilder extends WebDriverBuilder {

        WebDriver webDriver;
        public WebDriver build(String website){
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized");
            instantiateDriver(chromeOptions);
            webDriver.get(website);
            return webDriver;
        }

        private void instantiateDriver(ChromeOptions chromeOptions){
            WebDriverManager.chromiumdriver().setup();
            webDriver = new ChromeDriver(chromeOptions);
            webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        }

    }
}
