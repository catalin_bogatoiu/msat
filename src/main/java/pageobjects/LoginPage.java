package pageobjects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.base.BasePage;

public class LoginPage extends BasePage {

    @FindBy(name = "email")
    private WebElement emailInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[contains(text(), 'Sign in')]")
    private WebElement loginButton;

    @FindBy(css = "h6.MuiTypography-root")
    private WebElement loginErrorMessage;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    @Step(value = "Verifying if the login page is displayed.")
    protected void isPageDisplayed() {
        waitForVisibility(emailInput);
        waitForVisibility(passwordInput);
        waitForVisibility(loginButton);
    }

    @Step(value = "Input username: {username} and password: {password} and click the login button. *expect error")
    public LoginPage loginAndExpectError(String username, String password) {
        emailInput.sendKeys(username);
        passwordInput.sendKeys(password);
        loginButton.click();
        return this;
    }

    @Step(value = "Input username: {username} and password: {password} and click the login button.")
    public DashboardPage login(String username, String password) {
        emailInput.sendKeys(username);
        passwordInput.sendKeys(password);
        loginButton.click();
        return new DashboardPage(getWebDriver());
    }

    @Step(value = "Getting the login error message")
    public String getLoginErrorMessage() {
        return loginErrorMessage.getText();
    }
}
