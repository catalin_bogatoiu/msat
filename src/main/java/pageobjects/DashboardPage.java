package pageobjects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.base.BasePage;

import java.util.List;

public class DashboardPage extends BasePage {

    @FindBy(css = "div#site-layout div.container h5.MuiTypography-alignCenter")
    private List<WebElement> jobsForYouMessages;

    public DashboardPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    @Step("Verifying if the Dashboard Page is displayed.")
    protected void isPageDisplayed() {
        waitForVisibilityOfElements(jobsForYouMessages);
        waitForTextVisibilityInElement(jobsForYouMessages.get(0), "Your jobs for today");
        waitForTextVisibilityInElement(jobsForYouMessages.get(1), "Jobs for you today");
    }
}
