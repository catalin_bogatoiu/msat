package pageobjects.base;

import lombok.Data;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

@Data
public abstract class BasePage {
    private WebDriver webDriver;

    public BasePage(WebDriver webDriver) {
        setWebDriver(webDriver);
        PageFactory.initElements(webDriver, this);
        isPageDisplayed();
    }

    protected abstract void isPageDisplayed();

    private FluentWait<WebDriver> fluentWait() {
        return new FluentWait<>(getWebDriver()).withTimeout(Duration.ofSeconds(15)).ignoring(NoSuchElementException.class);
    }

    protected void waitForVisibility(WebElement webElement) {
        fluentWait().until(visibilityOf(webElement));
    }

    protected void waitForTextVisibilityInElement(WebElement element, String text) {
        fluentWait().until(textToBePresentInElement(element, text));
    }

    protected void waitForVisibilityOfElements(List<WebElement> webElementList){
        fluentWait().until(visibilityOfAllElements(webElementList));
    }
}
