package utils;

import utils.model.UserModel;

public class CredentialsProvider {
    public static UserModel getAdminUser() {
        return UserModel.builder()
                .username(ResourcesProvider.getKeyAsString("beta-staging.user.admin.auto.username"))
                .password(ResourcesProvider.getKeyAsString("beta-staging.user.admin.auto.password"))
                .build();
    }
}
