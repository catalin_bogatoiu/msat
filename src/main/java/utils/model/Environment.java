package utils.model;

public enum Environment {
    BETA, PRODUCTION
}
