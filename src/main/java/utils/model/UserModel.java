package utils.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserModel {
    private String username;
    private String password;
}
