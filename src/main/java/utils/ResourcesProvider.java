package utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ResourcesProvider {

    private final static String fileName = "general.properties";

    public static String getKeyAsString(String key) {
        PropertiesConfiguration config = new PropertiesConfiguration();
        try {
            config.load(fileName);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return config.getString(key, "VALUE_NOT_FOUND");
    }

    public static Integer getKeyAsInteger(String key) {
        PropertiesConfiguration config = new PropertiesConfiguration();
        try {
            config.load(fileName);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return config.getInteger(key, -1);
    }
}
