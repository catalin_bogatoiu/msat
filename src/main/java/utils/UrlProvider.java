package utils;

import utils.model.Environment;

public class UrlProvider {
    public static String getUrl(Environment environment) {
        if (environment.equals(Environment.BETA))
            return ResourcesProvider.getKeyAsString("beta-staging.url");
        else if (environment.equals(Environment.PRODUCTION))
            return ResourcesProvider.getKeyAsString("production.url");
        else throw new RuntimeException("ENVIRONMENT NOT SUPPORTED");
    }
}
