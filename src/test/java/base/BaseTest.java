package base;

public abstract class BaseTest {

    public abstract void killDriver();

    public abstract void startDriver();
}
