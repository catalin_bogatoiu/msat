package base;

import lombok.Data;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pageobjects.LoginPage;
import setup.driver.WebDriverBuilder;
import utils.UrlProvider;
import utils.model.Environment;

public class BaseWebTest extends BaseTest {
    private WebDriver webDriver;
    private LoginPage loginPage;

    @Override
    @AfterMethod
    public void killDriver() {
        getWebDriver().quit();
    }

    @Override
    @BeforeMethod
    public void startDriver() {
        setWebDriver(WebDriverBuilder.forChrome().build(UrlProvider.getUrl(Environment.BETA)));
        setLoginPage(new LoginPage(getWebDriver()));
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(LoginPage loginPage) {
        this.loginPage = loginPage;
    }
}
