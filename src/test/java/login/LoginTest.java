package login;

import base.BaseWebTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import static utils.CredentialsProvider.*;

public class LoginTest extends BaseWebTest {

    @Test
    public void loginWithEmptyUsernameExpectError() {
        String errorMessage = getLoginPage()
                .loginAndExpectError("", "somethingWrong")
                .getLoginErrorMessage();

        Assert.assertEquals(errorMessage, "Email and password fields are required");
    }

    @Test
    public void loginWithEmptyPasswordExpectError() {
        String errorMessage = getLoginPage()
                .loginAndExpectError("admin", "")
                .getLoginErrorMessage();

        Assert.assertEquals(errorMessage, "Email and password fields are required");
    }

    @Test
    public void loginWithEmptyDataAndExpectError() {
        String errorMessage = getLoginPage()
                .loginAndExpectError("", "")
                .getLoginErrorMessage();

        Assert.assertEquals(errorMessage, "Email and password fields are required");
    }

    @Test
    public void loginWithGoodUsernameButBadPasswordExpectError() {
        String errorMessage = getLoginPage()
                .loginAndExpectError(getAdminUser().getUsername(), "BAD_PASSWORD")
                .getLoginErrorMessage();

        Assert.assertEquals(errorMessage, "Incorrect e-mail or password");
    }

    @Test
    public void loginWithBadUsernameAndGoodPasswordExpectError() {
        String errorMessage = getLoginPage()
                .loginAndExpectError("BAD_USERNAME", getAdminUser().getPassword())
                .getLoginErrorMessage();

        Assert.assertEquals(errorMessage, "Incorrect e-mail or password");
    }

    @Test
    public void loginHappyFlow() {
        getLoginPage()
                .login(getAdminUser().getUsername(), getAdminUser().getPassword());
    }

}
